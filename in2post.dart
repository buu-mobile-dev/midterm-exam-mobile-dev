/**
 * Anurak Yutthanawa
 * 63160015 B.Sc. Computer Science
 */

class Infix2Postfix {
  List<String> transform(List<String> args) {
    List<String> operators = [];
    List<String> postfix = [];
    for (String token in args) {
      if (isNumeric(token)) {
        postfix.add(token);
      } else if (token == "(") {
        operators.add(token);
      } else if (token == ")") {
        while (operators.last != "(") {
          postfix.add(operators.removeLast());
        }
        operators.removeWhere((element) => element == "(");
      } else if (!isNumeric(token)) {
        while (operators.isNotEmpty &&
            operators.last != "(" &&
            precedence(token) < precedence(operators.last)) {
          postfix.add(operators.removeLast());
        }
        operators.add(token);
      }
    }

    while (operators.isNotEmpty) {
      postfix.add(operators.removeLast());
    }
    return postfix;
  }

  int precedence(String op) {
    switch (op) {
      case "(":
      case ")":
      case "[":
      case "]":
        return 1;
      case "*":
      case "/":
      case "%":
        return 2;
      case "+":
      case "-":
        return 3;
      default:
    }
    return 10;
  }

  bool isNumeric(String token) {
    return double.tryParse(token) != null;
  }
}
