class EvaluatePostfix {
  int evaluatePostfix(List<String> args) {
    List<int> values = [];
    for (String token in args) {
      if (isNumeric(token)) {
        values.add(int.parse(token));
      } else {
        int right = values.removeLast();
        int left = values.removeLast();
        values.add(
          operate(
            right: right,
            left: left,
            operator: token,
          ),
        );
      }
    }
    return values.first;
  }

  int operate({
    required int right,
    required int left,
    required String operator,
  }) {
    switch (operator) {
      case "+":
        return right + left;
      case "-":
        return right - left;
      case "*":
        return right * left;
      case "/":
        return right ~/ left;
      default:
    }
    return 0;
  }

  bool isNumeric(String token) {
    return double.tryParse(token) != null;
  }
}
