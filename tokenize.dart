/**
 * Anurak Yutthanawa
 * 63160015 B.Sc. Computer Science
 * 
 * Note: Sorry for this brainless code, I don't know what exactly asked output.
 */
class Tokenizer {
  List<String> tokenize(List<String> args) {
    List<String> tokenize = [];
    for (String i in args) {
      tokenize.add(i);
    }
    print(tokenize);
    return tokenize;
  }

  bool isNumeric(String token) {
    return double.tryParse(token) != null;
  }
}
