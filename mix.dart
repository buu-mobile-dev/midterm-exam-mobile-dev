import 'evalpost.dart';
import 'in2post.dart';
import 'tokenize.dart';

void main(List<String> args) {
  dynamic value = Tokenizer().tokenize(args);
  value = Infix2Postfix().transform(args);
  int result = EvaluatePostfix().evaluatePostfix(value);
  print(result);
}
